package tpVacunacionUngs;


public class Persona implements Comparable<Persona> {
		
		int edad,dni,prioridad;
		boolean deRiesgo,Escencial,vacunado,TieneTurno;
		Persona(){
			
		}
		
		Persona(int dni,Fecha nac,boolean der,boolean esc) {
			setEdad(nac,Fecha.hoy());
			this.dni=dni;
			this.prioridad=4;
			this.deRiesgo=der;
			this.Escencial=esc;
			this.vacunado=false;
			
		}
		// INT EDAD 
	    // INT DNI --> UN NUMERO MAYOR A 0 PARA SER VALIDO
		// INT PRIORIDAD ---> UN NUMERO ENTRE 1 y 4 PARA SABER A QUE GRUPO PERTENECE
		// BOOLEAN VACUNADO,DERIESGO,ESCENCIAL
			
		//CHEQUEAR SI ES TRABAJADOR ESCENCIAL CON VARIABLE DE INSTANCIA	Y QUE VACUNAS PUEDE RECIBIR	PRIORIDAD 1
		public void  definirPrioridad() {
			 if(edad>=60)
					this.setPrioridad(3); //chequeo de la prioridad mas baja y si cumple las 2 anteriores y ademas es trabajador de la salud , se pone como prioridad 1
			if(this.deRiesgo)
					this.setPrioridad(2);
			if(this.Escencial)
				this.setPrioridad(1);	
		}
		//COMPROBAR SI TIENE TURNO , PARA USAR EN SIST. VACUNACION PARA ASIGNAR O NO UN TURNO
		boolean tieneTurno() {
				return TieneTurno;
		}
		//SI LA PERSONA TIENE TURNO Y HAY VACUNA DISPONIBLE VACUNAR , TAMBIEN DEBE CAMBIAR EL ESTADO DE LA VARIABLE VACUNADO SI ESTA VACUNADO Y CON CUAL VACUNA FUE VACUNADO
		public void vacunar(Vacuna vac) {
			if(this.tieneTurno())
				this.vacunado= true;
		}

		/*//UNA VEZ QUE PASA EL DIA SE VACIA LA LISTA DE ESPERA  Y SE VUELEN A LLENAR
		public Set<Persona> isVacunado() {
			Map<Integer,String> mapa = new HashMap<Integer, String>();
			return mapa;
		}*/
		@Override
		public int compareTo(Persona o) {
			return o.getPrioridad()-this.getPrioridad(); //comprar las prioridades con el compareTO 
			// si la persona o prioridad > a la persona con la que comparo  esto POSITIVO
			// Si la persona o proidad es = a la persona con la que comparo va a dar 0
			// si la persona o prioridad es < a la persona con la que comparo va a dar negativo
			
		}
		//--------------------------------
		public int getEdad() {
			return edad;
		}

		public void setEdad(Fecha fechanac, Fecha hoy) {
			int difAnio = hoy.anio() - fechanac.anio();
			int difMes = hoy.mes() - fechanac.mes();
			int difDia = hoy.dia() - fechanac.dia();
			if (difMes < 0 || (difMes == 0 && difDia < 0)) {
		        difAnio = difAnio - 1;
		    }
			edad=difAnio;
		}

		public int getDni() {
			return dni;
		}

		public void setDni(int dni) {
			this.dni = dni;
		}
		
		public int getPrioridad() {
			return prioridad;
		}
		
		public void setPrioridad(int prioridad) {
			this.prioridad = prioridad;
		}
		
		// PERSONA + VACUNADO + TIPODEVACUNA (SE OBTIENE UTILIZANDO LOS METODOS DE QUE GRUPO ES)
		public String toString(){
			StringBuilder sb = new StringBuilder();
			sb.append("El dni de la persona es ")
			  .append(this.getDni())
			  .append(" edad ")
			  .append(this.getEdad())
			  .append("Estado de vacunacion ")
			  .append(this.vacunado)
			  .append("Y su prioridad es : ")
			  .append(this.getPrioridad());
			return sb.toString();
		}
		
	
}
