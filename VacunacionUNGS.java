package tpVacunacionUngs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;




public class VacunacionUNGS {
	
//	Set<Vacuna> vencidas; // CADA VEZ QUE ENCONTRAMOS UNA VACUNA VENCIDA , SE AGREGA ACA COn NOMBRE
//	Set<Persona> Personas; //--> CADA DIA SE GENERA UNA NUEVA CANTIDAD DE PERSONAS
	HashMap<Integer,Persona> PersonasSinTurno;
	HashMap<Integer,Integer> PersonasConTurno ;
	HashMap<Integer,Integer> PersonasVacunadas ;
	ArrayList<VacunaT1> vacunasT1;
	ArrayList<VacunaT2> vacunasT2;
	ArrayList<Vacuna> vacunasVencidas; 
	Fecha fechaInicial;
	int vacunasDiarias,vacunasDadas,personaAgregada;
	String nombreCentro;
	
	public VacunacionUNGS(String nombreCentro, int capacidadVacunacionDiaria) {
		// usar queues
		this.nombreCentro=nombreCentro;
		fechaInicial = new Fecha(10, 6, 2021);
		vacunasDiarias=capacidadVacunacionDiaria;
	    PersonasSinTurno = new HashMap<Integer,Persona>();
	    PersonasConTurno = new HashMap<Integer,Integer>(); // en realidad la lista de espera es eso , veremos 
	    PersonasVacunadas = new HashMap<Integer,Integer>();
		vacunasT1 = new ArrayList<VacunaT1>();
		vacunasT2 = new ArrayList<VacunaT2>();
		vacunasVencidas = new ArrayList<Vacuna>();
		personaAgregada=0;

	}
	public void ingresarVacunas(String nombreVacuna,int cantidad,Fecha fechaIngreso) {
		if(cantidad<=0)
			throw new SecurityException();
		int i=0;
		while(cantidad>=i) {
		if(nombreVacuna.equals("Sputnik") || nombreVacuna.equals("Sinopharm") || nombreVacuna.equals("AstraZeneca") )
			vacunasT1.add(new VacunaT1(nombreVacuna,fechaIngreso));
		
		else if(nombreVacuna.equals("Moderna") || nombreVacuna.equals("Pfizer"))
			vacunasT2.add(new VacunaT2(nombreVacuna,fechaIngreso));
		i++;
		}
	}
	public int vacunasDisponibles() {
		return  vacunasT1.size()+vacunasT2.size()-2; // el 2 va porque estoy ingresando dos tipos de vacuna pero varia segun la cantidad que se ingresen
	}
	public int vacunasDisponibles(String nombreVacuna) {
		int i=0; // TENGO QUE RESTAR LA CANTIDAD DE TIPOS DE VACUNA QUE SE SUMARON
		if(nombreVacuna.equals("Sputnik") || nombreVacuna.equals("Sinopharm") || nombreVacuna.equals("AstraZeneca") ) { // SI NOMBRE VACUNA ES ALGUNO DE LOS DE TIPOT1 , CON UN FOR EACH CUENTO CADA APARECION Y LO GUARDO EN UNA VARIABLE INT QUE LUEGO VOY A DEVOLVER
			for (VacunaT1 elem : vacunasT1) {
				elem.getNombre().equals(nombreVacuna);
				i++;
			}
		}
		else if(nombreVacuna.equals("Moderna") || nombreVacuna.equals("Pfizer")) {
			for (VacunaT2 elem : vacunasT2) {
				elem.getNombre().equals(nombreVacuna);
				i++;
			}
		}
		return i; // verifico que tipo de vacuna es y si de ahi busco la cantidad de apariciones y devuelvo ese numero
		
	}
	public void inscribirPersona(int dni , Fecha nacimiento, boolean tienePadecimientos, boolean esEmpleadoSalud){	
		
		Persona per = new Persona(dni,nacimiento,tienePadecimientos,esEmpleadoSalud);
		PersonasSinTurno.put(personaAgregada, per);
		per.definirPrioridad();
		personaAgregada++;
		
	}
	public List<Persona> listaDeEspera(){
		ArrayList<Persona>  lista = new ArrayList<Persona>(); // creo un arraylist , luego lo recorro las personasSinTurno con un for each para agregarlos a la lista de espera
		for (Entry<Integer, Persona> ent: PersonasSinTurno.entrySet()) {
			//System.out.println("Persona "+ent.getKey()+", Dni: "+ent.getValue().getDni());
			lista.add(ent.getKey(), ent.getValue());
		
		}
		return lista;	 
		 
	}
	public void generarTurnos(Fecha fechaInicial) {
		 /* primero se verifica si hay turnos vencidos . en caso de haber turnos vencidos , la persona que no asistio al turno debe ser borrada del sistema
		 * y la vacuna reservada debe volver a estar disponible
		 * 
		 * segundo , se deben verificar si hay vacunas vencidas y quitarlas del sistema
		 * por ultimo
		 * se procede a asiganar los turnos a partir de la fecha inicial recibida segun lo especificado en la 1era parte
		 * cada vez que se registra un nuevo turno , la vacuna destinada a esa persona
		 * dejara de estar disponible . dado que estara reservada para ser aplicada el dia del turno
		 */
		Iterator<VacunaT2> it = vacunasT2.iterator();
		VacunaT2 elem;
		vacunasT2.get(0).setVencida(true);
		while(it.hasNext()) {
			elem = it.next();
			if (elem.isVencida()) {
				it.remove();
				vacunasVencidas.add(elem);
			}
				
		}
		
		
	}
	public List<Integer> turnosConFecha(Fecha fecha){
		List<Integer> lista = new LinkedList<Integer>();
		/*
		 * devuelve una lista con los dni de las personas que tienen turno asignado para la fecha pasada por parametro
		 * si no hay turnos asignados para ese dia , se debe devolver una lista vacia
		 * la cantidad de turnos no puede exceder la capacidad por dia de la UNGS
		 */
		return lista;
	}
	public void vacunarInscripto(int dni,Fecha fechaVacunacion) {
		/*
		 * dado el dni de la persona y la fecha de vacunacion
		 * se valida que este inscrpito y que tenga turno para ese dia
		 * - si tiene turno y esta inscripto se debe registrar la persona como vacunada y la vacuna se quita deposito
		 * - si no esta inscripto o no tiene turno ese dia se genera una excepcion
		 */
	}
	public Map<Integer,String> reporteVacunacion(){
		Map<Integer,String> mapa = new HashMap<Integer, String>(); 
		/*
		 * devuelve un diccionario donde
		 * - la clave es el dni de las personas vacunadas
		 * - y el valor es el nombre de la vacuna aplicada
		 */
		return mapa;
		
	}
	public Map<String,Integer> reporteVacunasVencidas(){
		Map<String,Integer> mapa = new HashMap<String,Integer>();
		/*
		 * devuelve en O(1) un diccionario:
		 * -clave: nombre de la vacuna:
		 * - valor : cantidad vacunas vencidas conocidas hasta el momento
		 */
		return mapa;
	}
		
	
	

	
	/*//SI LA PERSONA ESTA EN LA LISTA DE ESPERA , SE LA VACUNA
	public void asignarPersonas();
	//SI HAY LA VACUNA QUE NECESITA Y HAY PERSONA DISPONIBLE  se los elige
	public void elegirVacunados();
	// SI LA PERSONA FUE VACUNADA , SE LA QUITA DE LA LISTA y SE LA AGREGA AL REPORTE
	public void quitarDeLista();
	// CAMBIAR DE FECHA
	public void nuevaFecha();
	
	
	// SI UNA PERSONA NO TIENE TURNO , LE DA TURNO
	public void darTurno (Persona per,Fecha fecha) ;
	// TENIENDO LOS TURNOS DADOS DEL DIA , ORGANIZARLOS SEGUN LA PRIORIDAD  1>2>3>4
	public Set<Persona> ListaDeEspera ();
	// SI PERSONA FUE VACUNADO , SE BUSCA EL NOMBRE Y QUE VACUNA FUE APLICADA.
	String reporte ();
	*/
	
	
}
