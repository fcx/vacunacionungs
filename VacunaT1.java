package tpVacunacionUngs;


public class VacunaT1 extends Vacuna   {

	private String nombre;
	private boolean vencida;

	VacunaT1(String nombre, Fecha fecha){
		super();
		vencida=false;
		this.nombre = nombre;
	}
	
	@Override
	boolean aplicable(Persona per) {
		boolean apicable = false;
		if (per.getEdad()>= 60) { 
			nombre = "Sputnik";
			return true;
		}
		else if (per.getEdad()>18 && per.getEdad()<32) { // no se como entregar distintos string ya que las vacunas son pueden se las mismas para ambas personas
			nombre = "Sinopharm";
			return true;
		}
		else if (per.getEdad()>32 && per.getEdad()<60) { // no se como entregar distintos string ya que las vacunas son pueden se las mismas para ambas personas
			nombre = "Astrazeneca";
			return true; 
			
		}
			return apicable;
	}
	



    public String getNombre() {
		return nombre;
	}

	public boolean isVencida() {
		return vencida;
	}

	public void setVencida(boolean vencida) {
		this.vencida = vencida;
	}

	

	
  
}
