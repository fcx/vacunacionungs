package tpVacunacionUngs;


public class VacunaT2 extends Vacuna {
	
	
	private boolean vencida;
	private String nombre;
	private Fecha fabri;
	VacunaT2(String nombre,Fecha fecha){
		super();
		this.nombre = nombre;
		this.fabri=fecha;
	}

	

	@Override
	boolean aplicable(Persona per) {
		boolean aplicable = false;
		if(per.getEdad()>=18 && per.getEdad()<60 && !vencida) {
			nombre = "Moderna";
			return true;
		}
		else if(per.getEdad()>=60 && !vencida) {
				nombre = "Pfizer";
				return true;
		}
		return aplicable;
	}
	boolean vencimientoModerna(Fecha otra) {
		int diasdeDiferencia = this.getFabri().mes()-otra.mes();
		if(diasdeDiferencia < 60)//fecha de ingreso de vacuna
			return vencida=true;
		else
			return vencida=false;
	}
	
	boolean vencimientoPfizer(Fecha otra) {
		int diasdeDiferencia = this.getFabri().mes()-otra.mes();
		if(diasdeDiferencia < 30)//fecha de ingreso de vacuna
			return vencida=true;
		else
			return vencida=false;
	}

	

	

    /*
     * GETTERS & SETTERS
     */
	public Fecha getFabri() {
		return fabri;
	}


	public boolean isVencida() {
		return vencida;
	}

	public void setVencida(boolean vencida) {
		this.vencida = vencida;
	}


	public String getNombre() {
		return nombre;
	}



	



   
    
}
