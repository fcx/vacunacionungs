# VacunacionUNGS

A partir de 2021 la ungs comenzó a prestar servicios de centro de vacunación.A tales fines las autoridades nos pidieron ayuda para gestionar la administración de vacunas.

A la ungs van llegando vacunas(1): pfizer, Sputnik, Sinopharm, Moderna y Astrazeneca.Las vacunas Pfizer y Sputnik son para mayores de 60 y Sinopharm, Moderna y Astrazeneca para todo público. Pfizer y Moderna se almacenan a -18 grados centígrados mientras que las otras a 3 grados.

Se inscribe a la población mayor de 18 años que soliciten vacunarse. Los datos a registrar son el dni, la edad, si trabaja en salud y si tiene enfermedades preexistentes que lo convierta en persona de riesgo (tieneprioridad de vacunación).
La asignación de turnos se realiza con la siguiente prioridad:
1.Los trabajadores de la salud.
2.Mayores de 60 años,
3.Personas con enfermedades preexistentes.
4.El resto de la población.

El sistema debe poder generar turnos para vacunación teniendo en cuenta las vacunas disponibles y la prioridad definida anteriormente.A partir de una fecha se generan los turnos de manera consecutiva según la capacidad de vacunación diaria de la universidad, hasta que se acaben las vacunas disponibles o la 
población inscripta que aún no fue vacunada. Cada turno debe indicar la persona y la vacuna que le será aplicada.
Se debe generar un listado por fecha con las personas que tengan turno para esa fecha.También, se espera poder generar un reporte que informe las vacunas aplicadas y a que persona se aplicó cada una. Por último, es necesario conocer las personas que se encuentran en lista de espera.(1) Toda esta información no es real, es solo a finesdidácticos del TP. Por simplicidad asumimos que solo se aplica 1 dosis.

