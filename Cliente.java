package tpVacunacionUngs;

import java.util.Map.Entry;

public class Cliente {
	
	public static void main(String[] args) {
		Fecha fTurnos = new Fecha(15, 7, 2021);
		VacunacionUNGS centro = new VacunacionUNGS("Universidad Nacional General Sarmiento ", 5);

		System.out.println("------------ Creacion -------------");
		System.out.println(			centro.nombreCentro);
		System.out.println("-----------------------------------");
		System.out.println();

		centro.ingresarVacunas("Moderna",10, new Fecha(15,5,2021));
		centro.ingresarVacunas("Sputnik", 3, new Fecha(15,5,2021));

		centro.inscribirPersona(34701000, new Fecha(1, 5, 1989), false, false);
		centro.inscribirPersona(29959000, new Fecha(20, 11, 1982), false, true);
		centro.inscribirPersona(24780201, new Fecha(1, 6, 1972), true, false);
		centro.inscribirPersona(29223000, new Fecha(2, 5, 1982), false, true);
		centro.inscribirPersona(13000000, new Fecha(1, 5, 1958), true, false);
		centro.inscribirPersona(13000050, new Fecha(20, 6, 1958), false, true);


		
		centro.generarTurnos(fTurnos);

		System.out.println("-------------- Turnos -------------");
		System.out.println(		centro.turnosConFecha(fTurnos));
		System.out.println("-----------------------------------");
		System.out.println();
		
		centro.vacunarInscripto(24780201, fTurnos);
		centro.vacunarInscripto(13000000, fTurnos);

		System.out.println("------------- Centro --------------");
		System.out.println(			centro.nombreCentro);
		System.out.println("-----------------------------------");

//		for (int i=0; i<centro.PersonasSinTurno.size(); i++) {
//			System.out.print(" Persona sin turno pero agregada al sistema Dni numero : " + centro.PersonasSinTurno.get(i).getDni());
//			System.out.print(" Y tiene una prioridad de " + centro.PersonasSinTurno.get(i).getPrioridad());
//			System.out.println();
//		}
		System.out.println("Lista de espera " + centro.listaDeEspera().size());

		
//		Iterator<VacunaT2> it = centro.vacunasT2.iterator();
//		VacunaT2 elem;
//		while(it.hasNext()) {
//			elem = it.next();
//			if (elem.isVencida()) 
//				it.remove();
//			
//		}
	
	
		
		
		System.out.println("Vacunas que no estan vencidas : "+ centro.vacunasDiarias);
		System.out.println("Vacunas disponibles diarias : " + (centro.vacunasDiarias-centro.vacunasVencidas.size()));
		System.out.println("Vacunas vencidas : " +centro.vacunasVencidas.size());
	

	}
}


