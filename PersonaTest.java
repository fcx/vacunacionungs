package tpVacunacionUngs;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonaTest {

	@Before
	public void setUp() throws Exception {	
	}

	@Test
	public void TestPrioridad() {
		Persona per = new Persona(59,new Fecha(1,1,1970),false,false);
		Persona perMayor = new Persona(66,new Fecha(1,1,1910),false,false);
		Persona perEsc = new Persona(30,new Fecha(1,1,1970),true,false);
		Persona perRiesgo = new Persona(30,new Fecha(1,1,1980),false,true);
		Persona RiesgoYEsc = new Persona(30,new Fecha(1,1,1989),true,true);
		
		per.definirPrioridad();
		perMayor.definirPrioridad();
		perRiesgo.definirPrioridad();
		perEsc.definirPrioridad();
		RiesgoYEsc.definirPrioridad();
		
		assertTrue("4",per.getPrioridad() ==  4);
		assertFalse("3",perMayor.getPrioridad() == 4);
		assertFalse("2",perEsc.getPrioridad() == 4);
		assertFalse("1",perRiesgo.getPrioridad() == 4);
		assertFalse("1",RiesgoYEsc.getPrioridad() == 4);
		
		System.out.println("Ciudadano comun " + per.getPrioridad());
		System.out.println("Persona Mayor " + perMayor.getPrioridad());
		System.out.println("Persona de Riesgo " +perEsc.getPrioridad());
		System.out.println("Persona Escencial " +perRiesgo.getPrioridad());
		System.out.println("Persona de Riesgo y Escencial "+ RiesgoYEsc.getPrioridad());
		
	}
	
	@Test
	public void TieneNoTurno() {
		Persona per = new Persona(59,new Fecha(1,1,2000),false,false);
		
		per.definirPrioridad();
		per.tieneTurno();
		System.out.println("------------------");
		System.out.println(per.tieneTurno());
		assertTrue("false",per.vacunado== false);
	}

}
